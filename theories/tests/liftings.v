From iris.proofmode Require Import tactics.
From iris_logrel Require Import logrel.
From iris.program_logic Require Import hoare.
From iris_logrel Require Import examples.counter.

Section liftings.
  Context `{logrelG Σ}.

  (* left-side hoare triples *)
  Definition lhs_ht (P : iProp Σ) (e : expr) (Q : val → iProp Σ) Δ Γ :=
    (∀ K t τ, □(P -∗ (∀ v, Q v -∗ {Δ;Γ} ⊨ fill K (of_val v) ≤log≤ t : τ)
        -∗ {Δ;Γ} ⊨ fill K e ≤log≤ t : τ))%I.

  Lemma lift_ht (P : iProp Σ) (e : expr) (Q : val → iProp Σ) E Δ Γ `{Closed ∅ e} :
    ({{ P }} e @ E {{ Q }}
     → lhs_ht P e Q Δ Γ)%I.
  Proof.
    iIntros "#He" (K t τ).
    iAlways. iIntros "HP"; iSpecialize ("He" with "HP").
    iIntros "Hlog".
    iApply bin_log_related_wp_l.
    iApply (wp_mask_mono _ E); first done.
    by iApply (wp_wand with "He").
  Qed.

  (* Definition taken from iris-atomic, strengthened a bit *)
  Definition atomic_triple {A : Type}
             (α: A → iProp Σ) (* atomic pre-condition *)
             (β: A → val → iProp Σ) (* atomic post-condition *)
             (Ei Eo: coPset) (* inside/outside masks *)
             (e: expr) : iProp Σ :=
    (∀ P Q, (P ={Eo, Ei}=> ∃ x:A,
               α x ∗
                 ((α x ={Ei, Eo}=∗ P) ∧
                  (∀ v, β x v ={Ei, Eo}=∗ Q v))
            ) -∗ {{ P }} e @ Eo {{ Q }})%I.

  (* left-side logically atomic triples *)
  (* Eo = E1, Ei = E2 *)
  (* TODO MASKS, outside mask is now always Top *)
  Definition atomic_logrel {A : Type}
             (α: A → iProp Σ) (* atomic pre-condition *)
             (β: A → val → iProp Σ) (* atomic post-condition *)
             (Ei : coPset) (* inside/outside masks *)
             (e: expr)
             Δ Γ : iProp Σ :=
    (∀ K t τ R2 R1, (R2 ∗ □ (|={⊤,Ei}=> ∃ (x : A),
                 α x ∗ R1 x ∗
                 ((α x ∗ R1 x ={Ei, ⊤}=∗ True) ∧
                  (∀ v, β x v ∗ R1 x ∗ R2 -∗ {Ei;Δ;Γ} ⊨ fill K (of_val v) ≤log≤ t : τ))))
                -∗ {Δ;Γ} ⊨ fill K e ≤log≤ t : τ)%I.

  (* Example: *)
  (* We can prove the atomic specification for the counter *)
  Lemma counter_atomic x E Δ Γ :
    atomic_logrel
      (fun (n : nat) => x ↦ᵢ #n)%I
      (fun (n : nat) (v : val) => ⌜v = #n⌝ ∗ x ↦ᵢ #(S n))%I
      E
      (FG_increment #x)
      Δ Γ.
  Proof.
    iIntros (K t τ R2 R1) "[HR2 #H]".
    iLöb as "IH".
    rewrite {2}/FG_increment. unlock. simpl.
    rel_rec_l.
    iPoseProof "H" as "H2". (* lolwhat *)
    rel_load_l_atomic.
    iMod "H" as (n) "[Hx [HR Hrev]]".  iModIntro.
    iExists #n. iFrame. iNext. iIntros "Hx".
    iDestruct "Hrev" as "[Hrev _]".
    iMod ("Hrev" with "[HR Hx]") as "_"; first by iFrame.
    rel_rec_l. rel_op_l. rel_cas_l_atomic.
    iMod "H2" as (n') "[Hx [HR HQ]]". iModIntro. simpl.
    destruct (decide (n = n')); subst.
    - iExists #n'. iFrame.
      iSplitR; eauto. { iDestruct 1 as %Hfoo. exfalso. done. }
      iIntros "_ !> Hx". simpl.
      iDestruct "HQ" as "[_ HQ]".
      iSpecialize ("HQ" $! (#n') with "[Hx HR HR2]"); first by iFrame.
      rel_if_true_l. by iApply "HQ".
    - iExists #n'. iFrame.
      iSplitL; eauto; last first.
      { iDestruct 1 as %Hfoo. exfalso. simplify_eq. }
      iIntros "_ !> Hx". simpl.
      rel_if_false_l.
      iDestruct "HQ" as "[HQ _]".
      iMod ("HQ" with "[Hx HR]") as "_"; first iFrame.
      rewrite /FG_increment. unlock. simpl.
      by iApply "IH".
  Qed.

  Lemma lift_atomic_tpl {A : Type}
             (α: A → iProp Σ) (* atomic pre-condition *)
             (β: A → val → iProp Σ) (* atomic post-condition *)
             (e : expr) (E : coPset) `{Closed ∅ e} Δ Γ :
      atomic_triple α β E ⊤ e -∗
      atomic_logrel α β E e Δ Γ.
  Proof.
    iIntros "Ht". rewrite /atomic_triple.
    iIntros (K t τ R2 R1) "[Hframe #H]".
    rewrite bin_log_related_eq /bin_log_related_def.
    iIntros (vvs ρ) "#Hspec #HΓ".
    iIntros (j K') "Hj /=".
    rewrite /env_subst !fill_subst.
    rewrite !Closed_subst_p_id.
    iModIntro.
    wp_bind_core (subst_ctx (fst <$> vvs) K).
    iApply ("Ht" $! (R2 ∗ j ⤇ fill K' (subst_p (snd <$> vvs) t))%I with "[] [Hframe Hj]"); last iFrame.
    iAlways. iIntros "[Hframe Hj]".
    iMod "H" as (a) "(Hα & HR & H)".
    iModIntro. iExists _. iFrame "Hα".
    iSplit.
    - iDestruct "H" as "[H _]".
      iIntros "Hα". iFrame. iApply "H". iFrame.
    - iDestruct "H" as "[_ H]".
      iIntros (v) "Hβ".
      iSpecialize ("H" $! v with "[Hβ HR Hframe]"); first iFrame.
      iSpecialize ("H" with "Hspec [HΓ] Hj"); first eauto.
      rewrite /interp_expr /= fill_subst Closed_subst_p_id.
      done.
  Qed.

  (* We can lift a Hoare triple to an atomic triple in which the inner mask is ⊤ *)
  Lemma LA_lift {A : Type} α (e : expr) β Δ Γ :
    (∀ (x : A), lhs_ht (α x) e (β x) Δ Γ) -∗
    atomic_logrel α β ⊤ e Δ Γ.
  Proof.
    rewrite /atomic_logrel.
    iIntros "#HT" (K t τ).
    iIntros (R2 R1) "[HR2 #Hlog]".
    iMod "Hlog" as (x) "(Hα & HR & Hm)".
    iApply ("HT" $! x K t τ with "Hα").
    iIntros (v) "Hβ".
    iDestruct "Hm" as "[_ Hm]".
    iApply "Hm". by iFrame.
  Qed.

End liftings.
