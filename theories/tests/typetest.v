From iris_logrel.F_mu_ref_conc Require Import typing reflection notation.

Definition prog : val := λ: "x", if: !"x" then (λ: <>, #1) else (λ: <>, #0).

Lemma prog_typed Γ : Γ ⊢ₜ prog : TArrow (Tref TBool) (TArrow TUnit TNat).
Proof.
  solve_typed.
Qed.

Hint Resolve prog_typed : typeable.

Definition prog2 : val := λ: <>,
  let: "x" := ref #false in 
  prog "x" #().

Lemma prog2_typed Γ : Γ ⊢ₜ prog2 : TArrow TUnit TNat.
Proof. solve_typed. Qed.

Hint Resolve prog2_typed : typeable.

