From iris.proofmode Require Import tactics.
From iris_logrel Require Import logrel.
From iris.program_logic Require Import hoare.

Section rr.
  Context `{logrelG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).

  Notation "P ∗-∗ Q" := ((P -∗ Q) ∗ (Q -∗ P))%I (at level 50).

  (* Question: Why cannot I use the internal equality here?
     Would like to have ≡ intead of ↔ or ∗-∗
   *)
  Lemma interp_val_arrow τ σ Δ (v v' : val) ρ :
    spec_ctx ρ -∗
    ⟦ τ → σ ⟧ Δ (v, v')
              ∗-∗
    (□ (∀ (w w' : val), ⟦ τ ⟧ Δ (w, w')
      -∗ {Δ;∅} ⊨ v w ≤log≤ v' w' : σ))%I.
  Proof.
    iIntros "#Hspec".
    iSplitL.
    - iIntros "/= #Hvv !#".
      iIntros (w w') "#Hw".
      iApply related_ret.
      iApply ("Hvv" $! (w, w') with "Hw").
    - iIntros "#Hvv /= !#".
      iIntros ([w w']) "#Hww /=".
      iSpecialize ("Hvv" with "Hww").
      rewrite bin_log_related_eq /bin_log_related_def.
      iIntros (j K) "Hj /=".
      iSpecialize ("Hvv" $! ∅ ρ with "Hspec []").
      { iAlways. by iApply interp_env_nil. }
      rewrite /interp_expr /=.
      iSpecialize ("Hvv" $! j K).
      rewrite /env_subst !fmap_empty !subst_p_empty.
      by iMod ("Hvv" with "Hj").
  Qed.

End rr.
