From iris.proofmode Require Import tactics. 
From iris_logrel.logrel Require Export rules_threadpool proofmode.tactics_threadpool logrel_binary.
Set Default Proof Using "Type".
Import lang.

(**************************)
(* / tp_apply *)

Section test.
Context `{logrelG Σ}.

Definition bot := App (TApp (TLam (Rec "f" "x" (App (Var "f") (Var "x"))))) #().
Notation Lam x e := (Rec BAnon x e).
Notation LamV x e := (RecV BAnon x e).

Lemma bot_test E K j ρ :
  nclose specN ⊆ E →
  spec_ctx ρ -∗
  j ⤇ fill K bot ={E}=∗ True.
Proof.
  iIntros (?) "#? Hj".
  unfold bot.
  tp_tlam j; tp_normalise j.  
  tp_rec j; tp_normalise j.
  tp_rec j; tp_normalise j.
  tp_rec j; tp_normalise j.
  done.
Qed.

Lemma alloc_test E K j (n : nat) ρ :
  nclose specN ⊆ E →
  spec_ctx ρ -∗
  j ⤇ fill K (App (Lam "x" (Store (Var "x") (#n + #10))) (Alloc #n)) ={E}=∗ True.
Proof.
  iIntros (?) "#? Hj".
  tp_alloc j as l. Undo.
  tp_alloc j as l "Hl". tp_normalise j.
  tp_pure j.
  tp_pure j (BinOp _ _ _). tp_normalise j.
  tp_store j.
  done.
Qed.

Lemma test1 E1 j K l (n : nat) ρ :
  nclose specN ⊆ E1 →
  j ⤇ fill K (App (Lam "x" (Store (Lit (Loc l)) (BinOp Add #1 (Var "x")))) (Load (# l))) -∗
  spec_ctx ρ -∗
  l ↦ₛ #n ={E1}=∗ True.
Proof.
  iIntros (?) "Hj #? Hl".
  tp_load j. tp_normalise j.
  tp_rec j. tp_normalise j.
  tp_op j. tp_normalise j.
  tp_store j. 
  done.
Qed.

Lemma test2 E j K (l : loc) (n : nat) ρ :
  nclose specN ⊆ E →
  j ⤇ fill K 
   (Fork 
     (Fork 
       (App (If (CAS (# l) (# n) (# (n+1))) (Lam "x" (Fst (Var "x"))) (Lam "x" (Snd (Var "x"))))
            (Pair (# 1) (# 2))))) -∗
  spec_ctx ρ -∗
  l ↦ₛ #n ={E}=∗ ∃ j, j ⤇ #1.
Proof.
  iIntros (?) "Hj #? Hl".
  tp_fork j as i "Hi". Undo.
  tp_fork j as i. Undo.
  tp_fork j. iIntros (i) "Hi".
  tp_fork i as k "Hk". tp_normalise i.
  tp_normalise j.
  tp_cas_suc k; tp_normalise k.
  tp_if_true k; tp_normalise k. 
  tp_rec k. tp_normalise k.
  tp_fst k; tp_normalise k.
  iExists k.
  done.
Qed.
End test.

Section test2.
Context `{logrelG Σ}.
(* TODO: Coq complains if I make it a section variable *)
Axiom (steps_release_test : forall E ρ j K (l : loc) (b : bool),
    nclose specN ⊆ E →
    spec_ctx ρ -∗ l ↦ₛ #b -∗ j ⤇ fill K (App #() #l)
    ={E}=∗ j ⤇ fill K #() ∗ l ↦ₛ #false).

Theorem test_apply E ρ j (b : bool) K l:
  nclose specN ⊆ E →
  spec_ctx ρ -∗ 
  l ↦ₛ #b -∗ j ⤇ fill K (App #() #l)
  -∗ |={E}=> True.
Proof.
  iIntros (?) "#Hs Hst Hj".
  tp_apply j steps_release_test with "Hst" as "Hl".
  done.
Qed.

End test2.
