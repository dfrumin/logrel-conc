From iris.program_logic Require Export weakestpre lifting.
From iris.proofmode Require Export tactics coq_tactics.
From iris_logrel.F_mu_ref_conc Require Export rules lang reflection.
Set Default Proof Using "Type".
Import lang.

(** * General tactics *)

(** Applied to goals that are equalities of expressions. Will try to unlock the
    LHS once if necessary, to get rid of the lock added by the syntactic sugar. *)
Ltac solve_of_val_unlock := try apply of_val_unlock; fast_done.
Hint Extern 0 (of_val _ = _) => solve_of_val_unlock : typeclass_instances.

(** A "finisher" tactic *)
Ltac tac_rel_done := split_and?; try
  lazymatch goal with
  | [ |- of_val _ = _ ] => solve_of_val_unlock
  | [ |- Closed _ _ ] => solve_closed
  | [ |- to_val _ = Some _ ] => solve_to_val
  | [ |- language.to_val _ = Some _ ] => solve_to_val
  | [ |- is_Some (to_val _)] => solve_to_val
  end.

(** ** Bending and binding *)
(** The tactic [reshape_expr e tac] decomposes the expression [e] into an
evaluation context [K] and a subexpression [e']. It calls the tactic [tac K e']
for each possible decomposition until [tac] succeeds. *)
Ltac reshape_val e tac :=
  let rec go e :=
  lazymatch e with
  | of_val ?v => v
  | Rec ?f ?x ?e => constr:(RecV f x e)
  | TLam ?e => constr:(TLamV e)
  | Lit ?l => constr:(LitV l)
  | Pair ?e1 ?e2 =>
    let v1 := go e1 in let v2 := go e2 in constr:(PairV v1 v2)
  | InjL ?e => let v := go e in constr:(InjLV v)
  | InjR ?e => let v := go e in constr:(InjRV v)
  | Fold ?e => let v := go e in constr:(FoldV v)
  | Pack ?e => let v := go e in constr:(PackV v)
  end in let v := go e in tac v.

Ltac reshape_expr e tac :=
  let rec go K e :=
  tac K e + match e with
  | App ?e1 ?e2 => reshape_val e1 ltac:(fun v1 => go (AppRCtx v1 :: K) e2)
  | App ?e1 ?e2 => go (AppLCtx e2 :: K) e1
  | BinOp ?op ?e1 ?e2 =>
     reshape_val e1 ltac:(fun v1 => go (BinOpRCtx op v1 :: K) e2)
  | BinOp ?op ?e1 ?e2 => go (BinOpLCtx op e2 :: K) e1
  | If ?e0 ?e1 ?e2 => go (IfCtx e1 e2 :: K) e0
  | Pair ?e1 ?e2 => reshape_val e1 ltac:(fun v1 => go (PairRCtx v1 :: K) e2)
  | Pair ?e1 ?e2 => go (PairLCtx e2 :: K) e1
  | Fst ?e => go (FstCtx :: K) e
  | Snd ?e => go (SndCtx :: K) e
  | InjL ?e => go (InjLCtx :: K) e
  | InjR ?e => go (InjRCtx :: K) e
  | Case ?e0 ?e1 ?e2 => go (CaseCtx e1 e2 :: K) e0
  | Fold ?e => go (FoldCtx :: K) e
  | Unfold ?e => go (UnfoldCtx :: K) e
  | TApp ?e => go (TAppCtx :: K) e
  | Pack ?e => go (PackCtx :: K) e 
  | Unpack ?e1 ?e2 => go (UnpackLCtx e2 :: K) e1
  | Alloc ?e => go (AllocCtx :: K) e
  | Load ?e => go (LoadCtx :: K) e
  | Store ?e1 ?e2 => reshape_val e1 ltac:(fun v1 => go (StoreRCtx v1 :: K) e2)
  | Store ?e1 ?e2 => go (StoreLCtx e2 :: K) e1
  | CAS ?e0 ?e1 ?e2 => reshape_val e0 ltac:(fun v0 => first
     [ reshape_val e1 ltac:(fun v1 => go (CasRCtx v0 v1 :: K) e2)
     | go (CasMCtx v0 e2 :: K) e1 ])
  | CAS ?e0 ?e1 ?e2 => go (CasLCtx e1 e2 :: K) e0
  end in go (@nil ectx_item) e.

(** wp-specific helper tactics *)
(* TODO: duplication? *)
Lemma tac_wp_bind `{heapG Σ} K Δ E Φ e :
  envs_entails Δ (WP e @ E {{ v, WP fill K (of_val v) @ E {{ Φ }} }})%I →
  envs_entails Δ (WP fill K e @ E {{ Φ }}).
Proof. rewrite /envs_entails => ->. by apply: wp_bind. Qed.
Lemma tac_wp_bind'' `{heapG Σ} K Δ E E1 E2 Φ e :
  envs_entails Δ (|={E1,E2}=> WP e @ E {{ v, WP fill K (of_val v) @ E {{ Φ }} }})%I →
  envs_entails Δ (|={E1,E2}=> WP fill K e @ E {{ Φ }}).
Proof. rewrite /envs_entails => ->. by apply fupd_mono, wp_bind. Qed.

Ltac wp_bind_core K := 
  lazymatch eval hnf in K with
  | [] => idtac
  | _ => apply (tac_wp_bind K); simpl
  end.

Ltac wp_bind_core'' K :=
  lazymatch eval hnf in K with
  | [] => idtac
  | _ => apply (tac_wp_bind'' K)
  end.

Tactic Notation "wp_bind" open_constr(efoc) :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp _ ?E ?e ?Q) => reshape_expr e ltac:(fun K e' =>
    unify e' efoc; wp_bind_core K) 
  | |- envs_entails _ (|={?E1,?E2}=> wp _ ?E ?e ?Q) => reshape_expr e ltac:(fun K e' =>
    unify e' efoc; wp_bind_core'' K)
  | _ => fail "wp_bind: not a 'wp'"
  end.

(** * Symbolic execution WP tactics *)
(** ** Pure reductions *)
Lemma tac_wp_pure `{heapG Σ} K φ e1 e2 ℶ1 ℶ2 E Φ :
  PureExec φ e1 e2 →
  φ →
  MaybeIntoLaterNEnvs 1 ℶ1 ℶ2 →
  envs_entails ℶ2 (WP fill K e2 @ E {{ Φ }}) →
  envs_entails ℶ1 (WP fill K e1 @ E {{ Φ }}).
Proof.
  rewrite /envs_entails=> ??? Hℶ2.
  rewrite into_laterN_env_sound /=.
  rewrite Hℶ2 -wp_pure_step_later //.
Qed.

Lemma tac_wp_value `{heapG Σ} Δ E Φ e v :
  IntoVal e v →
  envs_entails Δ (Φ v) → envs_entails Δ (WP e @ E {{ Φ }}).
Proof. rewrite /envs_entails=> ? ->. by apply wp_value. Qed.

Ltac wp_value := eapply tac_wp_value; [apply _|lazy beta].

Tactic Notation "wp_pure" open_constr(efoc) :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp _ ?E ?e ?Q) => reshape_expr e ltac:(fun K e' =>
    unify e' efoc;
    eapply (tac_wp_pure K);
    [apply _ (* PureExec *)
    |try (exact I || reflexivity || tac_rel_done)
    |apply _ (* IntoLaters *)
    |simpl_subst/=; try wp_value (* new goal *)])
  end.

Tactic Notation "wp_rec" := wp_pure (App (Rec _ _ _) _) || wp_pure (App _ _).
Tactic Notation "wp_seq" := wp_rec.
Tactic Notation "wp_let" := wp_rec.
Tactic Notation "wp_fst" := wp_pure (Fst (Pair _ _)).
Tactic Notation "wp_snd" := wp_pure (Snd (Pair _ _)).
Tactic Notation "wp_proj" := wp_pure (_ (Pair _ _)).
Tactic Notation "wp_case_inl" := wp_pure (Case (InjL _) _ _).
Tactic Notation "wp_case_inr" := wp_pure (Case (InjR _) _ _).
Tactic Notation "wp_case" := wp_pure (Case _ _ _).
Tactic Notation "wp_binop" := wp_pure (BinOp _ _ _).
Tactic Notation "wp_op" := wp_binop.
Tactic Notation "wp_if_true" := wp_pure (If #true _ _).
Tactic Notation "wp_if_false" := wp_pure (If #false _ _).
Tactic Notation "wp_if" := wp_pure (If _ _ _).
Tactic Notation "wp_unfold" := wp_pure (Unfold (Fold _)).
Tactic Notation "wp_fold" := wp_unfold.
Tactic Notation "wp_tlam" := wp_pure (TApp (TLam _)).
Tactic Notation "wp_unpack" := wp_pure (Unpack (Pack _) _).
Tactic Notation "wp_pack" := wp_unpack.

(** ** Stateful reductions *)
(* WARNING: Most of the code below is copypasta from heap_lang *)

Section heap.
Context `{heapG Σ}.
Implicit Types P Q : iProp Σ.
Implicit Types Φ : val → iProp Σ.
Implicit Types Δ : envs (iResUR Σ).
Import uPred.
Lemma tac_wp_alloc Δ Δ' E j e v Φ :
  to_val e = Some v →
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  (∀ l, ∃ Δ'',
    envs_app false (Esnoc Enil j (l ↦ᵢ v)) Δ' = Some Δ'' ∧
    (envs_entails Δ'' (Φ (LitV (Loc l))))) →
  envs_entails Δ (WP Alloc e @ E {{ Φ }}).
Proof.
  intros ?? HΔ. eapply wand_apply; first exact: wp_alloc.
  rewrite left_id into_laterN_env_sound; apply later_mono, forall_intro=> l.
  destruct (HΔ l) as (Δ''&?&HΔ'). rewrite envs_app_sound //; simpl.
  by rewrite right_id HΔ'.
Qed.

Lemma tac_wp_load Δ Δ' E i l q v Φ :
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  envs_lookup i Δ' = Some (false, l ↦ᵢ{q} v)%I →
  envs_entails Δ' (Φ v) →
  envs_entails Δ (WP Load (Lit (Loc l)) @ E {{ Φ }}).
Proof.
  intros. eapply wand_apply; first exact: wp_load.
  rewrite into_laterN_env_sound -later_sep envs_lookup_split //; simpl.
  by apply later_mono, sep_mono_r, wand_mono.
Qed.

Lemma tac_wp_store Δ Δ' Δ'' E i l v e v' Φ :
  to_val e = Some v' →
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  envs_lookup i Δ' = Some (false, l ↦ᵢ v)%I →
  envs_simple_replace i false (Esnoc Enil i (l ↦ᵢ v')) Δ' = Some Δ'' →
  envs_entails Δ'' (Φ (LitV LitUnit)) →
  envs_entails Δ (WP Store (Lit (Loc l)) e @ E {{ Φ }}).
Proof.
  intros. eapply wand_apply; first by eapply wp_store.
  rewrite into_laterN_env_sound -later_sep envs_simple_replace_sound //; simpl.
  rewrite right_id. by apply later_mono, sep_mono_r, wand_mono.
Qed.

Lemma tac_wp_cas_fail Δ Δ' E i l q v e1 v1 e2 v2 Φ :
  to_val e1 = Some v1 → to_val e2 = Some v2 →
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  envs_lookup i Δ' = Some (false, l ↦ᵢ{q} v)%I → v ≠ v1 →
  envs_entails Δ' (Φ (LitV (Bool false))) →
  envs_entails Δ (WP CAS (Lit (Loc l)) e1 e2 @ E {{ Φ }}).
Proof.
  intros. eapply wand_apply; first exact: wp_cas_fail.
  rewrite into_laterN_env_sound -later_sep envs_lookup_split //; simpl.
  by apply later_mono, sep_mono_r, wand_mono.
Qed.

Lemma tac_wp_cas_suc Δ Δ' Δ'' E i l v e1 v1 e2 v2 Φ :
  to_val e1 = Some v1 → to_val e2 = Some v2 →
  MaybeIntoLaterNEnvs 1 Δ Δ' →
  envs_lookup i Δ' = Some (false, l ↦ᵢ v)%I → v = v1 →
  envs_simple_replace i false (Esnoc Enil i (l ↦ᵢ v2)) Δ' = Some Δ'' →
  envs_entails Δ'' (Φ (LitV (Bool true))) →
  envs_entails Δ (WP CAS (Lit (Loc l)) e1 e2 @ E {{ Φ }}).
Proof.
  intros; subst. eapply wand_apply; first exact: wp_cas_suc.
  rewrite into_laterN_env_sound -later_sep envs_simple_replace_sound //; simpl.
  rewrite right_id. by apply later_mono, sep_mono_r, wand_mono.
Qed.
End heap.

Tactic Notation "wp_alloc" ident(l) "as" constr(H) :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp _ ?E ?e ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' =>
         match eval hnf in e' with Alloc _ => wp_bind_core K end)
      |fail 1 "wp_alloc: cannot find 'Alloc' in" e];
    eapply tac_wp_alloc with _ H _;
      [let e' := match goal with |- to_val ?e' = _ => e' end in
       tac_rel_done || fail "wp_alloc:" e' "not a value"
      |apply _
      |first [intros l | fail 1 "wp_alloc:" l "not fresh"];
        eexists; split;
          [env_cbv; reflexivity || fail "wp_alloc:" H "not fresh"
          |]]
  | _ => fail "wp_alloc: not a 'wp'"
  end.

Tactic Notation "wp_alloc" ident(l) :=
  let H := iFresh in wp_alloc l as H.

Tactic Notation "wp_load" :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp _ ?E ?e ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' =>
         match eval hnf in e' with Load _ => wp_bind_core K end)
      |fail 1 "wp_load: cannot find 'Load' in" e];
    eapply tac_wp_load;
      [apply _
      |let l := match goal with |- _ = Some (_, (?l ↦ᵢ{_} _)%I) => l end in
       iAssumptionCore || fail "wp_load: cannot find" l "↦ ?"
      |]
  | _ => fail "wp_load: not a 'wp'"
  end.

Tactic Notation "wp_store" :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp _ ?E ?e ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' =>
         match eval hnf in e' with Store _ _ => wp_bind_core K end)
      |fail 1 "wp_store: cannot find 'Store' in" e];
    eapply tac_wp_store;
      [let e' := match goal with |- to_val ?e' = _ => e' end in
       tac_rel_done || fail "wp_store:" e' "not a value"
      |apply _
      |let l := match goal with |- _ = Some (_, (?l ↦ᵢ{_} _)%I) => l end in
       iAssumptionCore || fail "wp_store: cannot find" l "↦ ?"
      |env_cbv; reflexivity
      |]
  | _ => fail "wp_store: not a 'wp'"
  end.

Tactic Notation "wp_cas_fail" :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp _ ?E ?e ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' =>
         match eval hnf in e' with CAS _ _ _ => wp_bind_core K end)
      |fail 1 "wp_cas_fail: cannot find 'CAS' in" e];
    eapply tac_wp_cas_fail;
      [let e' := match goal with |- to_val ?e' = _ => e' end in
       tac_rel_done || fail "wp_cas_fail:" e' "not a value"
      |let e' := match goal with |- to_val ?e' = _ => e' end in
       tac_rel_done || fail "wp_cas_fail:" e' "not a value"
      |apply _
      |let l := match goal with |- _ = Some (_, (?l ↦ᵢ{_} _)%I) => l end in
       iAssumptionCore || fail "wp_cas_fail: cannot find" l "↦ ?"
      |try congruence
      |]
  | _ => fail "wp_cas_fail: not a 'wp'"
  end.

Tactic Notation "wp_cas_suc" :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp _ ?E ?e ?Q) =>
    first
      [reshape_expr e ltac:(fun K e' =>
         match eval hnf in e' with CAS _ _ _ => wp_bind_core K end)
      |fail 1 "wp_cas_suc: cannot find 'CAS' in" e];
    eapply tac_wp_cas_suc;
      [let e' := match goal with |- to_val ?e' = _ => e' end in
       tac_rel_done || fail "wp_cas_suc:" e' "not a value"
      |let e' := match goal with |- to_val ?e' = _ => e' end in
       tac_rel_done || fail "wp_cas_suc:" e' "not a value"
      |apply _
      |let l := match goal with |- _ = Some (_, (?l ↦ᵢ{_} _)%I) => l end in
       iAssumptionCore || fail "wp_cas_suc: cannot find" l "↦ ?"
      |try congruence
      |env_cbv; reflexivity
      |]
  | _ => fail "wp_cas_suc: not a 'wp'"
  end.
