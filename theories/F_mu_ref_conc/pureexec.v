From iris_logrel.prelude Require Export hax.
From iris_logrel.F_mu_ref_conc Require Export lang subst notation.

Local Ltac solve_exec_safe := intros; subst; do 3 eexists; econstructor; eauto.
Local Ltac solve_exec_puredet := simpl; intros; by inv_head_step.
Local Ltac solve_pure_exec :=
  repeat lazymatch goal with
  | H: IntoVal ?e _ |- _ => rewrite -(of_to_val e _ into_val); clear H
  end;
  apply det_head_step_pure_exec; [ solve_exec_safe | solve_exec_puredet ].
Local Hint Resolve to_of_val.

Instance pure_binop op e1 v1 e2 v2 v `{!IntoVal e1 v1} `{!IntoVal e2 v2} :
  PureExec (binop_eval op v1 v2 = Some v)
           (BinOp op e1 e2)
           (of_val v).
Proof. solve_pure_exec. Qed.

Instance pure_rec f x e1 e2 v2 `{Closed ∅ (Rec f x e1)} `{!IntoVal e2 v2} :
  PureExec True
           (App (Rec f x e1) e2)
           (subst' f (Rec f x e1) (subst' x e2 e1)).
Proof. solve_pure_exec. Qed.

(* TODO: give this one a correct priority? *)
Instance pure_rec_locked e f x e1 e2 v2 `{!IntoVal e2 v2} `(e = Rec f x e1) `{Closed (x :b: f :b: ∅) e1} :
  PureExec True
           (App e e2)
           (e $/ v2) | 100.
Proof.
  apply det_head_step_pure_exec.
  - solve_exec_safe.
  - rewrite -(of_to_val e2 _ into_val).
    destruct f; solve_exec_puredet.
Qed.

Instance pure_fst e1 v1 e2 v2 `{!IntoVal e1 v1} `{!IntoVal e2 v2} :
  PureExec True (Fst (Pair e1 e2)) e1.
Proof. solve_pure_exec. Qed.

Instance pure_snd e1 v1 e2 v2 `{!IntoVal e1 v1} `{!IntoVal e2 v2} :
  PureExec True (Snd (Pair e1 e2)) e2.
Proof. solve_pure_exec. Qed.

Instance pure_unfold e1 v1 `{!IntoVal e1 v1} :
  PureExec True (Unfold (Fold e1)) e1.
Proof. solve_pure_exec. Qed.

Instance pure_unpack e1 e2 v1 `{!IntoVal e1 v1} :
  PureExec True (Unpack (Pack e1) e2) (e2 e1).
Proof. solve_pure_exec. Qed.

Instance pure_if_true e1 e2 :
  PureExec True (If #true e1 e2) e1.
Proof. solve_pure_exec. Qed.

Instance pure_if_false e1 e2 :
  PureExec True (If #false e1 e2) e2.
Proof. solve_pure_exec. Qed.

Instance pure_case_inl e0 v `{!IntoVal e0 v} e1 e2  :
  PureExec True (Case (InjL e0) e1 e2) (e1 e0).
Proof. solve_pure_exec. Qed.

Instance pure_case_inr e0 v `{!IntoVal e0 v} e1 e2  :
  PureExec True (Case (InjR e0) e1 e2) (e2 e0).
Proof. solve_pure_exec. Qed.

Instance pure_tlam e `{Closed ∅ e} :
  PureExec True
           (TApp (TLam e))
           e.
Proof. solve_pure_exec. Qed.
