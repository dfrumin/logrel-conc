From iris.proofmode Require Import tactics.
From iris_logrel.F_mu_ref_conc Require Export context_refinement.
From iris_logrel.logrel Require Export fundamental_binary.
From Autosubst Require Import Autosubst_Classes. (* for [subst] *)

Ltac fold_interp :=
  match goal with
  | |- context [ interp_expr (interp_arrow (interp ?A) (interp ?A'))
                            ?B (?C, ?D) ] =>
    change (interp_expr (interp_arrow (interp A) (interp A')) B (C, D)) with
    (interp_expr (interp (TArrow A A')) B (C, D))
  | |- context [ interp_expr (interp_prod (interp ?A) (interp ?A'))
                            ?B (?C, ?D) ] =>
    change (interp_expr (interp_prod (interp A) (interp A')) B (C, D)) with
    (interp_expr (interp (TProd A A')) B (C, D))
  | |- context [ interp_expr (interp_prod (interp ?A) (interp ?A'))
                            ?B (?C, ?D) ] =>
    change (interp_expr (interp_prod (interp A) (interp A')) B (C, D)) with
    (interp_expr (interp (TProd A A')) B (C, D))
  | |- context [ interp_expr (interp_sum (interp ?A) (interp ?A'))
                            ?B (?C, ?D) ] =>
    change (interp_expr (interp_sum (interp A) (interp A')) B (C, D)) with
    (interp_expr (interp (TSum A A')) B (C, D))
  | |- context [ interp_expr (interp_rec (interp ?A)) ?B (?C, ?D) ] =>
    change (interp_expr (interp_rec (interp A)) B (C, D)) with
    (interp_expr (interp (TRec A)) B (C, D))
  | |- context [ interp_expr (interp_forall (interp ?A))
                            ?B (?C, ?D) ] =>
    change (interp_expr (interp_forall (interp A)) B (C, D)) with
    (interp_expr (interp (TForall A)) B (C, D))
  | |- context [ interp_expr (interp_ref (interp ?A))
                            ?B (?C, ?D) ] =>
    change (interp_expr (interp_ref (interp A)) B (C, D)) with
    (interp_expr (interp (Tref A)) B (C, D))
  end.

Section bin_log_related_under_typed_ctx.
  Context `{logrelG Σ}.

  Ltac fundamental := simpl;
    try (solve_ndisj);
    lazymatch goal with
    | [ H : _ ⊢ₜ ?e : _ |- (envs_entails _ ({_;_;?Γ} ⊨ ?e ≤log≤ ?e : _))] =>
      let Z := iFresh in
      iPoseProof (binary_fundamental _ _ e _) as Z;
       [ solve_ndisj..
       | apply H
       | try iFrame Z ]
    end.
  Ltac solve_closed_typed :=
    lazymatch goal with
    | [ H : ?Γ ⊢ₜ ?e : _ |- Closed _ ?e ]
      => eapply (typed_X_closed Γ _ e H)
    end.

  (* Precongruence *)
  Lemma bin_log_related_under_typed_ctx Γ e e' τ Γ' τ' K :
    (Closed (dom _ Γ) e) →
    (Closed (dom _ Γ) e') →
    typed_ctx K Γ τ Γ' τ' →
    (□ (Γ ⊨ e ≤log≤ e' : τ) -∗
      Γ' ⊨ fill_ctx K e ≤log≤ fill_ctx K e' : τ')%I.
  Proof.
    revert Γ τ Γ' τ' e e'.
    induction K as [|k K]=> Γ τ Γ' τ' e e' H1 H2; simpl.
    - inversion_clear 1; trivial. iIntros "#H".
      iIntros (Δ). by iApply "H".
    - inversion_clear 1 as [|? ? ? ? ? ? ? ? Hx1 Hx2].
      specialize (IHK _ _ _ _ e e' H1 H2 Hx2).
      inversion Hx1; subst; simpl; try fold_interp; iIntros "#Hrel";
        iIntros (Δ).
      + iApply (bin_log_related_rec with "[-]"); auto;
        rewrite ?cons_binder_union.
        replace ({[x]} ∪ ({[f]} ∪ dom stringset _))
        with  (dom stringset (<[x:=τ0]> (<[f:=TArrow τ0 τ2]> Γ'))); last first.
        { by rewrite !dom_insert_binder. }
        eapply typed_ctx_closed; eauto.
        replace ({[x]} ∪ ({[f]} ∪ dom (gset string) _))
        with  (dom stringset (<[x:=τ0]> (<[f:=TArrow τ0 τ2]> Γ'))); last first.
        { by rewrite !dom_insert_binder. }
        eapply typed_ctx_closed; eauto.
        iAlways. iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_app with "[]"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_app _ _ _ _ _ _ τ2 with "[]"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_pair with "[]"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_pair with "[]"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply bin_log_related_fst.
        iApply (IHK with "[Hrel]"); auto.
      + iApply bin_log_related_snd.
        iApply (IHK with "[Hrel]"); auto.
      + iApply bin_log_related_injl.
        iApply (IHK with "[Hrel]"); auto.
      + iApply bin_log_related_injr.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_case with "[] []"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_case with "[] []"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_case with "[] []"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_if with "[] []"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_if with "[] []"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_if with "[] []"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_nat_binop with "[]"); try fundamental; eauto.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_nat_binop with "[]"); try fundamental; eauto.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_bool_binop with "[]"); try fundamental; eauto.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_bool_binop with "[]"); try fundamental; eauto.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_fold with "[]"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_unfold with "[]"); auto; try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_tlam with "[]"); try fundamental.
        (* TODO something wrong with setoids here *)
        replace (dom (gset string) _)
        with (dom (gset string) (subst (ren (+1)) <$> Γ')); last first.
        { unfold_leibniz. by rewrite !dom_fmap. }
        eapply typed_ctx_closed; eauto.
        replace (dom (gset string) _)
        with (dom (gset string) (subst (ren (+1)) <$> Γ')); last first.
        { unfold_leibniz. by rewrite !dom_fmap. }
        eapply typed_ctx_closed; eauto.
        iIntros (τi). iAlways.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_tapp' with "[]"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply bin_log_related_pack'.
        iApply (IHK with "[Hrel]"); auto.
      + iApply bin_log_related_unpack; try by (iIntros; fundamental).
        iApply (IHK with "[Hrel]"); auto.
      + iApply bin_log_related_unpack; try by (iIntros; fundamental).
        iIntros. iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_fork with "[]"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_alloc with "[]"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_load with "[]"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_store with "[]"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_store with "[]"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_CAS with "[] []"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_CAS with "[] []"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
      + iApply (bin_log_related_CAS with "[] []"); try fundamental.
        iApply (IHK with "[Hrel]"); auto.
  Qed.
End bin_log_related_under_typed_ctx.
