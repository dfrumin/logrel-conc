From iris.proofmode Require Import tactics.
From iris.algebra Require Import excl.
From iris_logrel Require Export logrel.

Definition newlock : val := λ: <>, ref #false.
Definition acquire : val := rec: "acquire" "x" :=
  if: CAS "x" #false #true
  then #()
  else "acquire" "x".

Definition release : val := λ: "x", "x" <- #false.
Definition with_lock : val := λ: "e" "l" "x",
  acquire "l";;
  let: "v" := "e" "x" in
  release "l";; "v".

Definition LockType := Tref TBool.

Hint Unfold LockType : typeable.

Lemma newlock_type Γ : typed Γ newlock (TArrow TUnit LockType).
Proof. solve_typed. Qed.

Hint Resolve newlock_type : typeable.

Lemma acquire_type Γ : typed Γ acquire (TArrow LockType TUnit).
Proof. solve_typed. Qed.

Hint Resolve acquire_type : typeable.

Lemma release_type Γ : typed Γ release (TArrow LockType TUnit).
Proof. solve_typed. Qed.

Hint Resolve release_type : typeable.

Lemma with_lock_type Γ τ τ' :
  typed Γ with_lock (TArrow (TArrow τ τ') (TArrow LockType (TArrow τ τ'))).
Proof. solve_typed. Qed.

Hint Resolve with_lock_type : typeable.

Class lockG Σ := LockG { lock_tokG :> inG Σ (exclR unitC) }.
Definition lockΣ : gFunctors := #[GFunctor (exclR unitC)].

Instance subG_lockΣ {Σ} : subG lockΣ Σ → lockG Σ.
Proof. solve_inG. Qed.

Section lockG_rules.
  Context `{!lockG Σ, !logrelG Σ} (N: namespace).

  Definition lock_inv (γ : gname) (l : loc) (R : iProp Σ) : iProp Σ :=
    (∃ b : bool, l ↦ᵢ #b ∗ if b then True else own γ (Excl ()) ∗ R)%I.

  Definition is_lock (γ : gname) (lk : val) (R : iProp Σ) : iProp Σ :=
    (∃ l: loc, ⌜lk = #l⌝ ∧ inv N (lock_inv γ l R))%I.

  Definition locked (γ : gname) : iProp Σ := own γ (Excl ()).

  Lemma locked_exclusive (γ : gname) : locked γ -∗ locked γ -∗ False.
  Proof. iIntros "H1 H2". by iDestruct (own_valid_2 with "H1 H2") as %?. Qed.

  Global Instance lock_inv_ne γ l : NonExpansive (lock_inv γ l).
  Proof. solve_proper. Qed.
  Global Instance is_lock_ne l : NonExpansive (is_lock γ l).
  Proof. solve_proper. Qed.

  Global Instance is_lock_persistent γ l R : Persistent (is_lock γ l R).
  Proof. apply _. Qed.
  Global Instance locked_timeless γ : Timeless (locked γ).
  Proof. apply _. Qed.

  Lemma bin_log_related_newlock_l (R : iProp Σ) Δ Γ K t τ :
    R -∗
    ▷(∀ (lk : loc) γ, is_lock γ #lk R
      -∗ ({Δ;Γ} ⊨ fill K #lk ≤log≤ t: τ)) -∗
    {Δ;Γ} ⊨ fill K (newlock #()) ≤log≤ t: τ.
  Proof.
    iIntros "HR Hlog".
    iApply bin_log_related_wp_l.
    rewrite -wp_fupd /newlock /=. unlock.
    wp_seq. wp_alloc l as "Hl".
    iMod (own_alloc (Excl ())) as (γ) "Hγ"; first done.
    iMod (inv_alloc N _ (lock_inv γ l R) with "[-Hlog]") as "#?".
    { iIntros "!>". iExists false. by iFrame. }
    iModIntro. iApply "Hlog". iExists l. eauto.
  Qed.

  Lemma bin_log_related_release_l (R : iProp Σ) (lk : loc) γ Δ Γ K t τ :
    is_lock γ #lk R -∗
    locked γ -∗
    R -∗
    ▷({Δ;Γ} ⊨ fill K #() ≤log≤ t: τ) -∗
    {Δ;Γ} ⊨ fill K (release #lk) ≤log≤ t: τ.
  Proof.
    iIntros "Hlock Hlocked HR Hlog".
    iDestruct "Hlock" as (l) "[% #?]"; simplify_eq.
    unlock release. simpl.
    rel_let_l.
    rel_store_l_atomic.
    iInv N as (b) "[Hl _]" "Hclose".
    iModIntro. iExists _. iFrame.
    iNext. iIntros "Hl".
    iMod ("Hclose" with "[-Hlog]").
    { iNext. iExists false. by iFrame. }
    iApply "Hlog".
  Qed.

  Lemma bin_log_related_acquire_l (R : iProp Σ) (lk : loc) γ Δ Γ K t τ :
    is_lock γ #lk R -∗
    ▷(locked γ -∗ R -∗ {Δ;Γ} ⊨ fill K #() ≤log≤ t: τ) -∗
    {Δ;Γ} ⊨ fill K (acquire #lk) ≤log≤ t: τ.
  Proof.
    iIntros "#Hlock Hlog".
    iLöb as "IH".
    unlock acquire. simpl.
    rel_rec_l.
    iDestruct "Hlock" as (l) "[% #?]". simplify_eq.
    rel_cas_l_atomic.
    iInv N as (b) "[Hl HR]" "Hclose".
    iModIntro. iExists _. iFrame.
    iSplit.
    - iIntros (?). iNext. iIntros "Hl".
      assert (b = true) as ->. { destruct b; congruence. }
      rel_if_l.
      iMod ("Hclose" with "[Hl]"); first (iNext; iExists true; eauto).
      by iApply "IH".
    - iIntros (?). simplify_eq.
      iNext. iIntros "Hl".
      rel_if_l.
      iMod ("Hclose" with "[Hl]"); first (iNext; iExists true; eauto).
      iDestruct "HR" as "[Hlocked HR]".
      iApply ("Hlog" with "Hlocked HR").
  Qed.

End lockG_rules.

Section lock_rules_r.
  Context `{logrelG Σ}.

  Variable (E : coPset).
  Variable (Δ : list (prodC valC valC -n> iProp Σ)).

  Lemma bin_log_related_newlock_r Γ K t τ 
    (Hcl : nclose specN ⊆ E) :
    (∀ l : loc, l ↦ₛ #false -∗ {E;Δ;Γ} ⊨ t ≤log≤ fill K #l : τ) -∗
    {E;Δ;Γ} ⊨ t ≤log≤ fill K (newlock #()): τ.
  Proof.
    iIntros "Hlog".
    unfold newlock. unlock.
    rel_rec_r.
    rel_alloc_r as l "Hl".
    iApply ("Hlog" with "Hl").
  Qed.

  Lemma bin_log_related_newlock_l_simp Γ K t τ :
    (∀ l : loc, l ↦ᵢ #false -∗ {Δ;Γ} ⊨ fill K #l ≤log≤ t : τ) -∗
    {Δ;Γ} ⊨ fill K (newlock #()) ≤log≤ t : τ.
  Proof.
    iIntros "Hlog".
    unfold newlock. unlock.
    rel_rec_l.
    rel_alloc_l as l "Hl".
    iApply ("Hlog" with "Hl").
  Qed.

  Global Opaque newlock.
  
  Transparent acquire. 
 
  Lemma bin_log_related_acquire_r Γ K l t τ
    (Hcl : nclose specN ⊆ E) :
    l ↦ₛ #false -∗
    (l ↦ₛ #true -∗ {E;Δ;Γ} ⊨ t ≤log≤ fill K Unit : τ) -∗
    {E;Δ;Γ} ⊨ t ≤log≤ fill K (acquire #l) : τ.
  Proof.
    iIntros "Hl Hlog".
    unfold acquire. unlock.
    rel_rec_r.
    rel_cas_suc_r. simpl.
    rel_if_r.
    by iApply "Hlog".
  Qed.

  Lemma bin_log_related_acquire_suc_l Γ K l t τ :
    l ↦ᵢ #false -∗
    (l ↦ᵢ #true -∗ {Δ;Γ} ⊨ fill K (#()) ≤log≤ t : τ) -∗
    {Δ;Γ} ⊨ fill K (acquire #l) ≤log≤ t : τ.
  Proof.
    iIntros "Hl Hlog".
    unfold acquire. unlock.
    rel_rec_l.
    rel_cas_l_atomic. iModIntro.
    iExists _; iFrame "Hl".
    iSplitR.
    { iIntros (Hfoo). congruence. }
    iIntros (_). iNext. iIntros "Hl".
    rel_if_l.
    by iApply "Hlog".
  Qed.

  Lemma bin_log_related_acquire_fail_l Γ K l t τ :
    l ↦ᵢ #true -∗
    (l ↦ᵢ #false -∗ {Δ;Γ} ⊨ fill K (acquire #l) ≤log≤ t : τ) -∗
    {Δ;Γ} ⊨ fill K (acquire #l) ≤log≤ t : τ.
  Proof.
    iIntros "Hl Hlog".
    iLöb as "IH".
    unfold acquire. unlock.
    rel_rec_l.
    rel_cas_l_atomic. iModIntro.
    iExists _; iFrame "Hl".
    iSplitL; last first.
    { iIntros (Hfoo). congruence. }
    iIntros (_). iNext. iIntros "Hl".
    rel_if_l.
    iApply ("IH" with "Hl Hlog").
  Qed.

  Global Opaque acquire.

  Transparent release.
  Lemma bin_log_related_release_r Γ K l t τ (b : bool)
    (Hcl : nclose specN ⊆ E) :
    l ↦ₛ #b -∗
    (l ↦ₛ #false -∗ {E;Δ;Γ} ⊨ t ≤log≤ fill K Unit : τ) -∗
    {E;Δ;Γ} ⊨ t ≤log≤ fill K (release #l) : τ.
  Proof.
    iIntros "Hl Hlog".
    unfold release. unlock.
    rel_rec_r.
    rel_store_r.
    by iApply "Hlog".
  Qed.

  Global Opaque release.
  
End lock_rules_r.
