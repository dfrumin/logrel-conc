From iris.proofmode Require Import tactics.
From iris_logrel Require Import logrel.
From iris_logrel.examples.stack Require Import
  CG_stack FG_stack refinement.

Section Mod_refinement.
  Context `{HLR : logrelG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).
  Implicit Types Δ : listC D.
  Import lang.

  Program Definition sint τi : prodC valC valC -n> iProp Σ := λne vv,
    (∃ (l stk stk' : loc), ⌜(vv.2) = (#stk', #l)%V⌝ ∗ ⌜(vv.1) = #stk⌝
      ∗ inv (stackN .@ (stk,stk')) (sinv τi stk stk' l))%I.
  Solve Obligations with solve_proper.

  Instance sint_persistent τi vv : Persistent (sint τi vv).
  Proof. apply _. Qed.

  Lemma module_refinement Δ Γ :
    {Δ;Γ} ⊨ FG_stack.stackmod ≤log≤ CG_stack.stackmod : TForall $ TExists $ TProd (TProd
                                                 (TArrow TUnit (TVar 0))
                                                 (TArrow (TVar 0) (TSum TUnit (TVar 1))))
                                                 (TArrow (TVar 0) (TArrow (TVar 1) TUnit)).
  Proof.
    unlock FG_stack.stackmod CG_stack.stackmod.
    iApply bin_log_related_tlam; auto.
    iIntros (τi) "!#".
    iApply (bin_log_related_pack (sint τi)).
    do 3 rel_tlam_l.
    do 3 rel_tlam_r.
    repeat iApply bin_log_related_pair.
    - iApply bin_log_related_arrow; eauto.
      iAlways. iIntros (? ?) "_".
      rel_seq_l.
      rel_seq_r.
      rel_alloc_l as istk "Histk". simpl.
      rel_alloc_l as stk "Hstk".
      rel_alloc_r as stk' "Hstk'".
      rel_alloc_r as l "Hl".
      rel_vals.
      rewrite -persistent.
      iAssert (sinv τi stk stk' l) with "[-]" as "Hinv".
      { iExists _,_. iFrame.
        rewrite stack_link_unfold. iExists _; iSplitL; eauto. }
      iMod (inv_alloc (stackN.@(stk,stk')) with "[Hinv]") as "#Hinv".
      { iNext. iExact "Hinv". }
      iModIntro.
      iExists l, stk, stk'. eauto.
    - iApply bin_log_related_arrow_val; eauto.
      iAlways. iIntros (? ?) "#Hvv /=".
      iDestruct "Hvv" as (l stk stk') "(% & % & #Hinv)".
      simplify_eq/=.
      rel_let_l.
      rel_let_r.
      Transparent FG_pop CG_locked_pop.
      rel_rec_l.
      rel_proj_r; rel_rec_r.
      unlock CG_locked_pop. simpl_subst/=.
      rel_proj_r.
      rel_let_r.
      replace (#();; acquire # l ;; let: "v" := (CG_pop #stk') #() in release # l ;; "v")%E with ((CG_locked_pop $/ LitV (Loc stk') $/ LitV (Loc l)) #())%E; last first.
      { unlock CG_locked_pop. simpl_subst/=. reflexivity. }
      replace (TSum TUnit (TVar 1))
      with (TSum TUnit (TVar 0)).[ren (+1)] by done.
      iApply bin_log_related_weaken_2.
      iApply (FG_CG_pop_refinement' (stackN.@(stk,stk')) with "Hinv").
      solve_ndisj.
    - iApply bin_log_related_arrow_val; eauto.
      { unlock FG_push. done. }
      iAlways. iIntros (? ?) "#Hvv /=".
      iDestruct "Hvv" as (l stk stk') "(% & % & #Hinv)".
      simplify_eq/=.
      rel_let_r.
      Transparent FG_push.
      rel_rec_l.
      iApply bin_log_related_arrow_val; eauto.
      { unlock FG_push. simpl_subst/=. done. }
      { unlock FG_push. simpl_subst/=. solve_closed. }
      iAlways. iIntros (v v') "#Hτi /=".
      rel_let_r.
      rel_proj_r; rel_rec_r.
      unlock CG_locked_push. simpl_subst/=.
      rel_proj_r; rel_let_r.
      replace (let: "x" := v' in acquire # l ;; (CG_push #stk') "x" ;; release # l)%E
        with ((CG_locked_push $/ LitV stk' $/ LitV l) v')%E; last first.
      { unlock CG_locked_push. simpl_subst/=. done. }
      change TUnit with (TUnit.[ren (+1)]).
      iApply (FG_CG_push_refinement (stackN.@(stk,stk')) with "Hinv Hτi").
      solve_ndisj.
  Qed.
End Mod_refinement.

Theorem module_ctx_refinement :
  ∅ ⊨ FG_stack.stackmod ≤ctx≤ CG_stack.stackmod :
    TForall $ TExists $ TProd (TProd
                                 (TArrow TUnit (TVar 0))
                                 (TArrow (TVar 0) (TSum TUnit (TVar 1))))
                                 (TArrow (TVar 0) (TArrow (TVar 1) TUnit)).
Proof.
  eapply (logrel_ctxequiv logrelΣ); [solve_closed.. | intros ].
  apply module_refinement.
Qed.
