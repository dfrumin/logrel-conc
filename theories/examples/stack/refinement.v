From iris.proofmode Require Import tactics.
From iris_logrel Require Import logrel.
From iris_logrel.examples.stack Require Import CG_stack FG_stack.

Definition stackN : namespace := nroot .@ "stack".

Section Stack_refinement.
  Context `{logrelG Σ}.
  Notation D := (prodC valC valC -n> iProp Σ).
  Implicit Types Δ : listC D.
  Import lang.

  Notation DD := (prodC locC valC -n> iProp Σ).

  (** The "partial pointsto" proposition is duplicable *)
  Local Instance istk_fromand (istk : loc) (w : val) :
    FromAnd false (∃ q, istk ↦ᵢ{q} w) (∃ q, istk ↦ᵢ{q} w) (∃ q, istk ↦ᵢ{q} w).
  Proof.
    rewrite /FromAnd. iIntros "[H1 H2]".
    iDestruct "H1" as (q1) "H1". iDestruct "H2" as (q2) "H2".
    iCombine "H1 H2" as "H". eauto.
  Qed.
  Local Instance istk_intoand (istk : loc) (w : val) :
    IntoAnd false (∃ q, istk ↦ᵢ{q} w) (∃ q, istk ↦ᵢ{q} w) (∃ q, istk ↦ᵢ{q} w).
  Proof.
    rewrite /IntoAnd. iDestruct 1 as (q) "[H1 H2]".
    iSplitL "H1"; eauto.
  Qed.

  Program Definition stack_link_pre (τi : D) : DD -n> DD := λne P vv,
    (∃ w, (∃ q, vv.1 ↦ᵢ{q} w) ∗
      ( (⌜w = NONEV⌝ ∧ ⌜vv.2 = FoldV NONEV⌝)
      ∨ (∃ (y1 : val) (z1 : loc) (y2 z2 : val),
            ⌜w = SOMEV (PairV y1 (FoldV #z1))⌝
          ∗ ⌜vv.2 = FoldV (SOMEV (PairV y2 z2))⌝
          ∗ □ τi (y1, y2)
          ∗ ▷ P (z1, z2))))%I.
  Solve Obligations with solve_proper.

  Global Instance stack_link_pre_contractive τi : Contractive (stack_link_pre τi).
  Proof. solve_contractive. Qed.

  Definition stack_link (Q : D) : DD := fixpoint (stack_link_pre Q).

  Lemma stack_link_unfold (Q : D) (istk : loc) (v : val) :
    stack_link Q (istk, v) ≡
    (∃ w, (∃ q, istk ↦ᵢ{q} w) ∗
      ((⌜w = NONEV⌝ ∧ ⌜v = FoldV NONEV⌝)
      ∨ (∃ (y1 : val) (z1 : loc) (y2 z2 : val),
            ⌜w = SOMEV (PairV y1 (FoldV #z1))⌝
          ∗ ⌜v = FoldV (SOMEV (PairV y2 z2))⌝
          ∗ □ Q (y1, y2)
          ∗ ▷ stack_link Q (z1, z2))))%I.
  Proof. by rewrite {1}/stack_link fixpoint_unfold. Qed.

  (** Actually, the whole `stack_link` predicate is duplicable *)
  Local Instance stack_link_intoand (Q : D) (istk : loc) (v : val) :
    IntoAnd false (stack_link Q (istk, v)) (stack_link Q (istk, v)) (stack_link Q (istk, v)).
  Proof.
    rewrite /IntoAnd. iLöb as "IH" forall (istk v).
    rewrite {1 2 3}stack_link_unfold.
    iDestruct 1 as (w) "([Histk Histk2] & [HLK | HLK])".
    - iDestruct "HLK" as "[% %]".
      iSplitL "Histk"; iExists _; iFrame; eauto.
    - iDestruct "HLK" as (y1 z1 y2 z2) "(% & % & #HQ & HLK)".
      iDestruct ("IH" with "HLK") as "[HLK HLK2]".
      iClear "IH".
      iSplitL "Histk HLK"; iExists _; iFrame; iRight; iExists _,_,_,_; eauto.
  Qed.

  Definition sinv (τi : D) stk stk' l' : iProp Σ :=
    (∃ (istk : loc) v,
       stk' ↦ₛ v
     ∗ l'   ↦ₛ #false
     ∗ stk  ↦ᵢ (FoldV #istk)
     ∗ stack_link τi (istk, v))%I.

  Ltac close_sinv Hcl asn :=
    iMod (Hcl with asn) as "_";
    [iNext; rewrite /sinv; iExists _,_; by iFrame |]; try iModIntro.

  Lemma FG_CG_push_refinement N st st' (τi : D) (v v' : val) l Γ :
    N ## logrelN →
    inv N (sinv τi st st' l) -∗ □ τi (v,v') -∗
    Γ ⊨ (FG_push $/ (LitV (Loc st))) v
       ≤log≤
        (CG_locked_push $/ (LitV (Loc st')) $/ (LitV (Loc l))) v' : TUnit.
  Proof.
    iIntros (?) "#Hinv #Hvv'". iIntros (Δ).
    Transparent FG_push.
    unfold FG_push. unlock. simpl_subst/=.
    iLöb as "IH".
    rel_rec_l.
    rel_load_l_atomic.
    iInv N as (istk w) "(>Hst' & >Hl & >Hst & HLK)" "Hclose".
    iExists (FoldV #istk). iFrame.
    iModIntro. iNext. iIntros "Hst".
    close_sinv "Hclose" "[Hst Hst' Hl HLK]". clear w.
    rel_rec_l.
    rel_alloc_l as nstk "Hnstk". simpl.
    rel_cas_l_atomic.
    iInv N as (istk' w) "(>Hst' & >Hl & >Hst & HLK)" "Hclose".
    iExists (FoldV #istk'). iFrame.
    iModIntro. iSplit.
    - (* CAS fails *)
      iIntros (?); iNext; iIntros "Hst".
      close_sinv "Hclose" "[Hst Hst' Hl HLK]". clear w.
      rel_if_false_l. simpl.
      rel_rec_l.
      by iApply "IH".
    - (* CAS succeeds *)
      iIntros (?). simplify_eq/=. iNext. iIntros "Hst".
      rel_apply_r (CG_push_r with "Hst' Hl").
      { solve_ndisj. }
      iIntros "Hst' Hl".
      iMod ("Hclose" with "[Hst Hst' Hl HLK Hnstk]").
      { iNext. rewrite {2}/sinv. iExists _,_.
        iFrame "Hst' Hst Hl".
        rewrite (stack_link_unfold _ nstk).
        iExists _. iSplitL "Hnstk".
        - iExists 1%Qp; iFrame.
        - iRight. iExists _,_,_,_. eauto. }
      rel_if_true_l.
      iApply bin_log_related_unit.
  Qed.

  Lemma FG_CG_pop_refinement' N st st' (τi : D) l Δ Γ :
    N ## logrelN →
    inv N (sinv τi st st' l) -∗
    {τi::Δ;Γ} ⊨ (FG_pop $/ LitV (Loc st)) #() ≤log≤ (CG_locked_pop $/ LitV (Loc st') $/ LitV (Loc l)) #() : TSum TUnit (TVar 0).
  Proof.
    Transparent CG_locked_pop FG_pop CG_pop.
    iIntros (?) "#Hinv".
    iLöb as "IH".
    rewrite {2}/FG_pop. unlock.  simpl_subst/=.

replace ((rec: "pop" "st" <> :=
             let: "stv" := ! "st" in
             let: "x" := ! (Unfold "stv") in
             match: "x" with
               InjL <> => InjL #()
             | InjR "x" =>
               let: "y" := Fst "x" in let: "ys" := Snd "x" in if: CAS "st" "stv" "ys" then InjR "y" else ("pop" "st") #()
             end))%E with
(of_val FG_pop) by (by rewrite /FG_pop; unlock).

    rel_rec_l.
    rel_load_l_atomic.
    iInv N as (istk w) "(>Hst' & >Hl & >Hst & HLK)" "Hclose".
    iExists _. iFrame.
    iModIntro. iNext. iIntros "Hst /=".
    rel_rec_l.
    rel_unfold_l.
    iDestruct "HLK" as "[HLK HLK2]".
    rewrite {1}stack_link_unfold.
    iDestruct "HLK" as (w') "(Histk & HLK)".
    iDestruct "HLK" as "[[% %] | HLK]"; simplify_eq/=.
    - (* The stack is empty *)
      rel_apply_r (CG_pop_fail_r with "Hst' Hl").
      { solve_ndisj. }
      iIntros "Hst' Hl".
      (* duplicate the top node *)
      iDestruct "Histk" as "[Histk Histk2]".
      close_sinv "Hclose" "[Hst' Hst Hl HLK2]".
      iDestruct "Histk2" as (q) "Histk2".
      rel_load_l. rel_let_l.
      rel_case_l. rel_let_l.
      rel_vals.
      { iModIntro. iLeft. iExists (_,_). eauto. }
    - (* The stack has a value *)
      iDestruct "HLK" as (y1 z1 y2 z2) "(% & % & #Hτ & HLK_tail)"; simplify_eq/=.
      (* duplicate the top node *)
      close_sinv "Hclose" "[Hst' Hst Hl HLK2]".
      iDestruct "Histk" as (q) "Histk".
      rel_load_l. rel_let_l.
      repeat (rel_pure_l _).
      rel_cas_l_atomic.
      iInv N as (istk' w) "(>Hst' & >Hl & >Hst & HLK)" "Hclose".
      iExists _. iFrame.
      iModIntro. iSplit.
      + (* CAS fails *) iIntros (?); simplify_eq/=.
        iNext. iIntros "Hst".
        rel_if_l.
        close_sinv "Hclose" "[Hst Hst' Hl HLK]".
        rel_rec_l.
        iApply "IH".
      + (* CAS succeeds *) iIntros (?); simplify_eq/=.
        iNext. iIntros "Hst".
        rel_if_l.
        rewrite (stack_link_unfold _ istk).
        iDestruct "HLK" as (w') "(Histk2 & HLK)".
        iAssert (⌜w' = InjRV (y1, FoldV #z1)⌝)%I with "[Histk Histk2]" as %->.
        { iDestruct "Histk2" as (?) "Histk2".
          iApply (mapsto_agree with "Histk2 Histk"). }
        iDestruct "HLK" as "[[% %] | HLK]"; first by congruence.
        iDestruct "HLK" as (? ? ? ? ? ?) "[#Hτ2 HLK]". simplify_eq/=.
        rel_apply_r (CG_pop_suc_r with "Hst' Hl").
        { solve_ndisj. }
        iIntros "Hst' Hl".
        close_sinv "Hclose" "[-]".
        rel_vals.
        { iModIntro. iRight.
          iExists (_,_). eauto. }
  Qed.

  Lemma FG_CG_pop_refinement st st' (τi : D) l N Δ Γ :
    N ## logrelN →
    inv N (sinv τi st st' l) -∗
    {τi::Δ;Γ} ⊨ FG_pop $/ LitV (Loc st) ≤log≤ CG_locked_pop $/ LitV (Loc st') $/ LitV (Loc l) : TArrow TUnit (TSum TUnit (TVar 0)).
  Proof.
    iIntros (?) "#Hinv".
    iApply bin_log_related_arrow_val; eauto.
    { unlock FG_pop CG_locked_pop. reflexivity. }
    { unlock FG_pop CG_locked_pop. reflexivity. }
    { unlock FG_pop CG_locked_pop. simpl_subst/=. solve_closed. }
    { unlock FG_pop CG_locked_pop. simpl_subst/=. solve_closed. }
    iAlways. iIntros (? ?) "[% %]". simplify_eq/=.
    iApply (FG_CG_pop_refinement' N); eauto.
  Qed.

  Lemma FG_CG_iter_refinement st st' (τi : D) l N Δ Γ:
    N ## logrelN →
    inv N (sinv τi st st' l) -∗
    {τi::Δ;Γ} ⊨ FG_read_iter $/ LitV (Loc st) ≤log≤ CG_snap_iter $/ LitV (Loc st') $/ LitV (Loc l) : TArrow (TArrow (TVar 0) TUnit) TUnit.
  Proof.
    iIntros (?) "#Hinv".
    Transparent FG_read_iter CG_snap_iter.
    unfold FG_read_iter, CG_snap_iter. unlock.
    simpl_subst/=.
    iApply bin_log_related_arrow; eauto.
    iAlways. iIntros (f1 f2) "#Hff /=".
    rel_rec_r.
    rel_rec_l.
    Transparent FG_iter CG_iter. unlock FG_iter CG_iter.
    rel_rec_l. rel_rec_r.
    Transparent CG_snap. unlock CG_snap.
    rel_rec_r. rel_rec_r. rel_rec_r.

    rel_load_l_atomic.
    iInv N as (istk w) "(>Hst' & >Hl & >Hst & HLK)" "Hclose".
    iExists _. iFrame.
    iModIntro. iNext. iIntros "Hst /=".

    rel_apply_r (bin_log_related_acquire_r with "Hl").
    { solve_ndisj. }
    iIntros "Hl /=".
    rel_rec_r.
    rel_load_r.
    rel_rec_r.
    rel_apply_r (bin_log_related_release_r with "Hl").
    { solve_ndisj. }
    iIntros "Hl /=".
    rel_rec_r. rel_let_r.
    rel_let_l.

    iDestruct "HLK" as "[HLK HLK2]".
    iMod ("Hclose" with "[Hst' HLK2 Hst Hl]") as "_".
    { iNext. iExists _, _. iFrame. }
    iLöb as "IH" forall (istk w).

    rewrite {1}stack_link_unfold.
    iDestruct "HLK" as (w') "([Histk Histk2] & HLK)".
    iDestruct "HLK" as "[[% %] | HLK]"; simplify_eq/=.
    - (* The stack is empty *)
      iDestruct "Histk2" as (q) "Histk2".
      rel_fold_r. rel_case_r. rel_rec_r.
      rel_fold_l. rel_load_l. rel_case_l.
      rel_let_l. iApply bin_log_related_unit.
    - (* The stack has a top element *)
      iDestruct "HLK" as (y1 z1 y2 z2) "(% & % & #Hτ & HLK_tail)"; simplify_eq/=.
      iDestruct "Histk2" as (q) "Histk2".
      rel_fold_r. rel_case_r. rel_rec_r. rel_proj_r.
      rel_fold_l. rel_load_l. rel_case_l.
      rel_let_l. rel_proj_l. rel_let_l. rel_proj_l. rel_let_l.
      iApply bin_log_related_seq'.
      { iApply bin_log_related_app; eauto. by rel_vals. }
      rel_rec_r. rel_proj_r. rel_let_r.
      rel_let_l. rel_let_l.
      iApply ("IH" with "HLK_tail").
  Qed.

End Stack_refinement.

Section Full_refinement.
  Local Ltac replace_l c :=
    lazymatch goal with
    | [|- envs_entails _ (bin_log_related _ _ _ ?e _ _) ] =>
      replace e with c; last first
    end.
  Local Ltac replace_r c :=
    lazymatch goal with
    | [|- envs_entails _ (bin_log_related _ _ _ _ ?e' _) ] =>
      replace e' with c; last first
    end.

  (* ∀ α. (α → Unit) * (Unit → Unit + α) * ((α → Unit) → Unit) *)
  Lemma FG_CG_stack_refinement `{logrelG Σ} Δ Γ :
     {Δ;Γ} ⊨ FG_stack ≤log≤ CG_stack : TForall (TProd (TProd
           (TArrow (TVar 0) TUnit)
           (TArrow TUnit (TSum TUnit (TVar 0))))
           (TArrow (TArrow (TVar 0) TUnit) TUnit)).
  Proof.
    unfold FG_stack. unfold CG_stack.
    iApply bin_log_related_tlam; auto.
    iIntros (τi) "!#".
    rel_alloc_r as l "Hl".
    rel_rec_r.
    rel_alloc_r as st' "Hst'".
    unlock CG_stack_body.
    repeat rel_rec_r.
    (* TODO: i have to do a bind before allocation, otherwise it doesn't pick the correct reduct. *)
    rel_bind_l (ref (InjL #()))%E.
    rel_alloc_l as istk "Histk".
    simpl.
    rel_alloc_l as st "Hst".
    simpl.
    rel_rec_l.
    iAssert (stack_link τi (istk, FoldV (InjLV Unit))) with "[Histk]" as "HLK".
    { rewrite stack_link_unfold.
      iExists _. iSplitL; simpl; eauto. }
    iAssert (sinv τi st st' l) with "[Hst Hst' HLK Hl]" as "Hinv".
    { iExists _, _. by iFrame. }
    iMod (inv_alloc stackN with "[Hinv]") as "#Hinv".
    { iNext. iExact "Hinv". }
    unlock FG_stack_body.
    unlock FG_push.
    repeat rel_rec_l.
    unlock FG_pop.
    repeat rel_rec_l. simpl_subst/=.
    unlock FG_read_iter.
    repeat rel_rec_l.

    unlock CG_locked_push. simpl_subst/=.
    repeat rel_rec_r.
    unlock CG_locked_pop. simpl_subst/=.
    repeat rel_rec_r.
    unlock CG_snap_iter. simpl_subst/=.
    repeat rel_rec_r.

    repeat iApply bin_log_related_pair.
    - iApply bin_log_related_arrow_val; eauto.
      iAlways. iIntros (v1 v2) "#Hτ /=".
      replace_l ((FG_push $/ LitV (Loc st)) v1)%E.
      { unlock FG_push. simpl_subst/=. reflexivity. }
      replace_r ((CG_locked_push $/ LitV (Loc st') $/ LitV (Loc l)) v2)%E.
      { unlock CG_locked_push. simpl_subst/=. reflexivity. }
      iApply (FG_CG_push_refinement with "Hinv Hτ").
      solve_ndisj.
    - replace_l (FG_pop $/ LitV (Loc st))%E.
      { unlock FG_pop. by simpl_subst/=. }
      replace_r (CG_locked_pop $/ LitV (Loc st') $/ LitV (Loc l))%E.
      { unlock CG_locked_pop. by simpl_subst/=. }
      iApply (FG_CG_pop_refinement with "Hinv").
      solve_ndisj.
    - replace_l (FG_read_iter $/ LitV (Loc st))%E.
      { unlock FG_read_iter. by simpl_subst/=. }
      replace_r (CG_snap_iter $/ LitV (Loc st') $/ LitV (Loc l))%E.
      { unlock CG_snap_iter. by simpl_subst/=. }
      iApply (FG_CG_iter_refinement with "Hinv").
      solve_ndisj.
  Qed.

  Theorem stack_ctx_refinement :
    ∅ ⊨ FG_stack ≤ctx≤ CG_stack :
      TForall (TProd (TProd (TArrow (TVar 0) TUnit)
                            (TArrow TUnit (TSum TUnit (TVar 0))))
                            (TArrow (TArrow (TVar 0) TUnit) TUnit)).
  Proof.
    eapply (logrel_ctxequiv logrelΣ); [solve_closed.. | intros ].
    apply FG_CG_stack_refinement.
  Qed.
End Full_refinement.
