From iris.proofmode Require Import tactics.
From iris_logrel Require Export logrel.
From iris_logrel.examples Require Import lock.

Definition CG_increment : val := λ: "x" "l",
  acquire "l";;
  let: "n" := !"x" in
  "x" <- #1 + "n";;
  release "l";;
  "n".

Definition counter_read : val := λ: "x" <>, !"x".

Definition CG_counter : val := λ: <>,
  let: "l" := newlock #() in
  let: "x" := ref #0 in
  ((λ: <>, CG_increment "x" "l"), counter_read "x").

Definition FG_increment : val := rec: "inc" "v" :=
  let: "c" := !"v" in
  if: CAS "v" "c" (#1 + "c")
  then "c"
  else "inc" "v".

Definition wkincr : val := λ: "l",
  "l" <- !"l" + #1.

Definition FG_counter : val := λ: <>,
  let: "x" := ref #0 in
  ((λ: <>, FG_increment "x"), counter_read "x").

Section CG_Counter.
  Context `{logrelG Σ}.
  Variable (Δ : list (prodC valC valC -n> iProp Σ)).

  (* Coarse-grained increment *)  
  Lemma CG_increment_type Γ :
    typed Γ CG_increment (TArrow (Tref TNat) (TArrow LockType TNat)).
  Proof. solve_typed. Qed.

  Hint Resolve CG_increment_type : typeable.

  Lemma bin_log_related_CG_increment_r Γ K E t τ (x l : loc) (n : nat) :
    nclose specN ⊆ E →
    (x ↦ₛ # n -∗ l ↦ₛ #false -∗
    (x ↦ₛ # (S n) -∗ l ↦ₛ #false -∗
      ({E;Δ;Γ} ⊨ t ≤log≤ fill K #n : τ)) -∗
    {E;Δ;Γ} ⊨ t ≤log≤ fill K (CG_increment #x #l) : τ)%I.
  Proof.
    iIntros (?) "Hx Hl Hlog".
    unfold CG_increment. unlock. simpl_subst/=.
    repeat rel_rec_r.
    rel_apply_r (bin_log_related_acquire_r with "Hl"); eauto.
    iIntros "Hl".
    rel_rec_r.
    rel_load_r.
    rel_let_r.
    rel_op_r.
    rel_store_r.
    rel_rec_r.
    rel_apply_r (bin_log_related_release_r with "Hl"); eauto.
    iIntros "Hl". rel_seq_r.
    by iApply ("Hlog" with "Hx Hl").
  Qed.

  Lemma bin_log_counter_read_r Γ E K x (n : nat) t τ
    (Hspec : nclose specN ⊆ E) :
    x ↦ₛ #n -∗
    (x ↦ₛ #n -∗ {E;Δ;Γ} ⊨ t ≤log≤ fill K #n : τ) -∗
    {E;Δ;Γ} ⊨ t ≤log≤ fill K ((counter_read $/ LitV (Loc x)) #()) : τ.
  Proof.
    iIntros "Hx Hlog".
    unfold counter_read. unlock. simpl.
    rel_rec_r.
    rel_load_r.
    by iApply "Hlog".
  Qed.
 
  Lemma counter_read_type Γ :
    typed Γ counter_read (TArrow (Tref TNat) (TArrow TUnit TNat)).
  Proof. solve_typed. Qed.

  Hint Resolve counter_read_type : typeable.

  Lemma CG_counter_type Γ :
    typed Γ CG_counter (TArrow TUnit (TProd (TArrow TUnit TNat) (TArrow TUnit TNat))).
  Proof. solve_typed. Qed.

  Hint Resolve CG_counter_type : typeable.

  (* Fine-grained increment *)
  Lemma FG_increment_type Γ :
    typed Γ FG_increment (TArrow (Tref TNat) TNat).
  Proof. solve_typed. Qed.

  Hint Resolve FG_increment_type : typeable.

  Lemma bin_log_related_FG_increment_r Γ K E t τ (x : loc) (n : nat) :
    nclose specN ⊆ E →
    (x ↦ₛ # n -∗
    (x ↦ₛ #(S n) -∗
      {E;Δ;Γ} ⊨ t ≤log≤ fill K #n : τ) -∗
    {E;Δ;Γ} ⊨ t ≤log≤ fill K (FG_increment #x) : τ)%I.
  Proof.
    iIntros (?) "Hx Hlog".
    unlock FG_increment. simpl_subst/=.
    rel_seq_r.
    rel_load_r; rel_seq_r.
    rel_op_r.
    rel_cas_suc_r.
    rel_if_r.
    by iApply "Hlog".
  Qed.

  Lemma FG_counter_type Γ :
    typed Γ FG_counter (TArrow TUnit (TProd (TArrow TUnit TNat) (TArrow TUnit TNat))).
  Proof. solve_typed. Qed.

  Hint Resolve FG_counter_type : typeable.

  Definition counterN : namespace := nroot .@ "counter".

  Definition counter_inv l cnt cnt' : iProp Σ :=
    (∃ n : nat, l ↦ₛ #false ∗ cnt ↦ᵢ #n ∗ cnt' ↦ₛ #n)%I.

  (* A logically atomic specification for
     a fine-grained increment with a baked in frame. *)
  Lemma bin_log_FG_increment_logatomic R P Γ E K x t τ  :
    P -∗
    □ (|={⊤,E}=> ∃ n : nat, x ↦ᵢ #n ∗ R n ∗
       ((x ↦ᵢ #n ∗ R n ={E,⊤}=∗ True) ∧
        (x ↦ᵢ #(S n) ∗ R n -∗ P -∗
            {E;Δ;Γ} ⊨ fill K #n ≤log≤ t : τ)))
    -∗ {Δ;Γ} ⊨ fill K (FG_increment #x) ≤log≤ t : τ.
  Proof.
    iIntros "HP #H".
    iLöb as "IH".
    rewrite {2}/FG_increment. unlock. simpl.
    rel_rec_l.
    iPoseProof "H" as "H2". (* lolwhat *)
    rel_load_l_atomic.
    iMod "H" as (n) "[Hx [HR Hrev]]".  iModIntro.
    iExists #n. iFrame. iNext. iIntros "Hx".
    iDestruct "Hrev" as "[Hrev _]".
    iMod ("Hrev" with "[HR Hx]") as "_"; first iFrame.
    rel_rec_l. rel_op_l. rel_cas_l_atomic.
    iMod "H2" as (n') "[Hx [HR HQ]]". iModIntro. simpl.
    destruct (decide (n = n')); subst.
    - iExists #n'. iFrame.
      iSplitR; eauto. { iDestruct 1 as %Hfoo. exfalso. done. }
      iIntros "_ !> Hx". simpl.
      iDestruct "HQ" as "[_ HQ]".
      iSpecialize ("HQ" with "[$Hx $HR]").
      rel_if_true_l. by iApply "HQ".
    - iExists #n'. iFrame. 
      iSplitL; eauto; last first.
      { iDestruct 1 as %Hfoo. exfalso. simplify_eq. }
      iIntros "_ !> Hx". simpl.
      rel_if_false_l.
      iDestruct "HQ" as "[HQ _]".
      iMod ("HQ" with "[$Hx $HR]").
      rewrite /FG_increment. unlock. simpl.
      by iApply "IH".
  Qed.

  (* A similar atomic specification for the counter_read fn *)
  Lemma bin_log_counter_read_atomic_l R P Γ E K x t τ :
    P -∗
    □ (|={⊤,E}=> ∃ n : nat, x ↦ᵢ #n ∗ R n ∗
       (x ↦ᵢ #n ∗ R n ={E,⊤}=∗ True) ∧
        (x ↦ᵢ #n ∗ R n -∗ P -∗
            {E;Δ;Γ} ⊨ fill K #n ≤log≤ t : τ))
    -∗ {Δ;Γ} ⊨ fill K ((counter_read $/ LitV (Loc x)) #()) ≤log≤ t : τ.
  Proof.
    iIntros "HP #H".
    unfold counter_read. unlock. simpl.
    rel_rec_l. rel_load_l_atomic.
    iMod "H" as (n) "[Hx [HR Hfin]]". iModIntro.
    iExists _; iFrame "Hx". iNext.
    iIntros "Hx".
    iDestruct "Hfin" as "[_ Hfin]".
    iApply ("Hfin" with "[Hx HR] HP"). by iFrame.
  Qed.

  Lemma FG_CG_increment_refinement l cnt cnt' Γ :
    inv counterN (counter_inv l cnt cnt') -∗
    {Δ;Γ} ⊨ FG_increment #cnt ≤log≤ CG_increment #cnt' #l : TNat.
  Proof.
    iIntros "#Hinv".
    rel_apply_l
      (bin_log_FG_increment_logatomic
              (fun n => (l ↦ₛ #false) ∗ cnt' ↦ₛ #n)%I
              True%I); first done.
    iAlways. iInv counterN as ">Hcnt" "Hcl". iModIntro.
    iDestruct "Hcnt" as (n) "(Hl & Hcnt & Hcnt')".
    iExists _; iFrame.
    iSplit.
    - iIntros "(Hcnt & Hl & Hcnt')".
      iApply ("Hcl" with "[-]").
      iNext. iExists _. iFrame.
    - iIntros "(Hcnt & Hl & Hcnt') _".
      rel_apply_r (bin_log_related_CG_increment_r with "Hcnt' Hl").
      { solve_ndisj. }
      iIntros "Hcnt' Hl".
      iMod ("Hcl" with "[-]").
      { iNext. iExists _. iFrame. }      
      by iApply bin_log_related_nat.
  Qed.

  Lemma counter_read_refinement l cnt cnt' Γ :
    inv counterN (counter_inv l cnt cnt') -∗
    {Δ;Γ} ⊨ counter_read #cnt ≤log≤ counter_read #cnt' : TArrow TUnit TNat.
  Proof.    
    iIntros "#Hinv".
    rel_rec_l. rel_rec_r.
    iApply bin_log_related_arrow_val.
    { unfold counter_read. unlock. simpl. reflexivity. }
    { unfold counter_read. unlock. simpl. reflexivity. }
    { unfold counter_read. unlock. simpl. solve_closed. }
    { unfold counter_read. unlock. simpl. solve_closed. }

    iAlways. iIntros (v v') "[% %]"; simpl in *; subst.
    rel_apply_l
      (bin_log_counter_read_atomic_l
         (fun n => (l ↦ₛ #false) ∗ cnt' ↦ₛ #n)%I
         True%I); first done.
    iAlways. iInv counterN as (n) "[>Hl [>Hcnt >Hcnt']]" "Hclose". 
    iModIntro. 
    iExists n. iFrame "Hcnt Hcnt' Hl".
    iSplit.
    - iIntros "(Hcnt & Hl & Hcnt')". iApply "Hclose".
      iNext. iExists n. by iFrame.
    - iIntros "(Hcnt & Hl & Hcnt') _ /=".
      rel_apply_r (bin_log_counter_read_r with "Hcnt'").
      { solve_ndisj. }
      iIntros "Hcnt'".
      iMod ("Hclose" with "[Hl Hcnt Hcnt']"); simpl.
      { iNext. iExists _. by iFrame. }
      by iApply bin_log_related_nat.
  Qed.

  Lemma FG_CG_counter_refinement :
    {Δ;∅} ⊨ FG_counter ≤log≤ CG_counter :
          TArrow TUnit (TProd (TArrow TUnit TNat) (TArrow TUnit TNat)).
  Proof.
    unfold FG_counter, CG_counter.
    iApply bin_log_related_arrow; try by (unlock; eauto).
    iAlways. iIntros (? ?) "/= ?"; simplify_eq/=.
    unlock. rel_rec_l. rel_rec_r.
    rel_apply_r bin_log_related_newlock_r; auto.
    iIntros (l) "Hl".
    rel_rec_r.
    rel_alloc_r as cnt' "Hcnt'".
    rel_alloc_l as cnt "Hcnt". simpl.

    rel_rec_l.
    rel_rec_r.

    (* establishing the invariant *)
    iAssert (counter_inv l cnt cnt')
      with "[Hl Hcnt Hcnt']" as "Hinv".
    { iExists _. by iFrame. }
    iMod (inv_alloc counterN with "[Hinv]") as "#Hinv"; trivial.

    iApply bin_log_related_pair.
    - iApply bin_log_related_arrow; eauto.
      iAlways. iIntros (??) "_". rel_seq_l; rel_seq_r.
      iApply (FG_CG_increment_refinement with "Hinv").
    - iApply (counter_read_refinement with "Hinv").
  Qed.
End CG_Counter.

Theorem counter_ctx_refinement :
  ∅ ⊨ FG_counter ≤ctx≤ CG_counter :
         TArrow TUnit (TProd (TArrow TUnit TNat) (TArrow TUnit TNat)).
Proof.
  eapply (logrel_ctxequiv logrelΣ); [solve_closed.. | intros ].
  apply FG_CG_counter_refinement.
Qed.
